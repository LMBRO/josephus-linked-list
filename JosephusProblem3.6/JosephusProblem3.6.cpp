// JosephusProblem3.6.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
/*
* Persons 0,1,2,...,n-1 sitting around in a circle. A hot potato is passed around the circle starting with person 1 and after m passes
* the person holding the potato is eliminated The circle then closes ranks and the person after the one that was just eliminated
* picks up the potato. This function returns the order of elimination for m and n.
* unsigned int n: The number of persons in the circle.
* unsigned int k - 1: The number of passes before the next person is eliminated.
* unsigned int k : The kth person including the starting person is eliminated.
*/
std::vector<unsigned int> josephus(unsigned int n, unsigned int k) {
	using std::list;
	using std::vector;
	using std::iota;

	std::vector<unsigned int> rvec{};
	rvec.reserve(n);

	list<unsigned int> l(n);
	iota(l.begin(), l.end(), 0);
	// I don't think pos will be needed//unsigned int pos = 0;
	list<unsigned int>::iterator it = l.begin();
	while (l.size() > 1) {
		for (int i = 0; i != (k - 1) % l.size(); ++i) {
			if (++it == l.end()) {
				it = l.begin();
			}
		}
		rvec.push_back(*it);
		it = l.erase(it);
		if (it == l.end())
			it = l.begin();
	}
	rvec.push_back(l.front());
	return rvec;
}

int main()
{
	for (auto e : josephus(6, 4))
		std::cout << e << "\t";
	return 0;
}

